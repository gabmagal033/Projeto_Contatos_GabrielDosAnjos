//
//  ViewController.swift
//  Projeto_Contatos_GabrielDosAnjos
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
    
}

class ViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome: "Contato1", numero: "31 98855-3548", email: "contato1@email.com", endereco: "Rua Obisidia, 11"))
        listaDeContatos.append(Contato(nome: "Contato2", numero: "31 96541-3548", email: "contato2@email.com", endereco: "Rua Obisidia, 13"))
        listaDeContatos.append(Contato(nome: "Contato3", numero: "31 95627-3548", email: "contato3@email.com", endereco: "Rua Obisidia, 15"))
        
    }


}

